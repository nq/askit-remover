# AskITRemover

## Overview

AskITRemover is a small program designed to permanently remove the AskIT icon on your desktop. Upon program start, the shortcut will be deleted. It then listens for file creation events in order to delete the file when it is recreated by some group policy script. 

## Usage
To build, install, and run: 
```
cd <project dir>
install.bat
```

To build it:
```
./mvnw package
```

Then run the generated jar file:
```
java -jar ./target/askit-remover-0.1.jar
```

Most of the program arguments are set by default, but you can also override them yourself:
```
java -jar <jar> [-i|--interactive] [-f|--filename <filename>] [-d|--watchdir <watchdir>] [-h|--help]
```
