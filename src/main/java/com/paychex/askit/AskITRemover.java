package com.paychex.askit;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.*;
import java.util.Scanner;

import static java.nio.file.StandardWatchEventKinds.*;

/**
 * DirWatcher2: Instead of using a FileTreeWalker and a WatchKey service to watch
 * for directory changes, this directory watch uses a directory stream directly
 * and iterates over the file tree in intervals.
 *
 * @author Wheeler
 */
public class AskITRemover {

    private Path watchDir;
    private String fileName;
    private WatchService watcher;
    private WatchKey key;

    private Thread daemon;

    private static final String DEFAULT_WATCH_DIR = Paths.get(System.getenv("PUBLIC"), "Desktop").toString();
    private static final String DEFAULT_FILE_NAME = "AskIT.url";

    final public static Logger logger = LoggerFactory.getLogger(new Object() {}.getClass().getEnclosingClass());

    /**
     * Create a new instance of the directory watcher. Initializes the data structure to
     * store the file listing, also creates a list of events.
     *
     * @param watchDir The directory that will be watched for files.
     */
    public AskITRemover(Path watchDir, String fileName) {
        this.watchDir = watchDir;
        this.fileName = fileName;
        try {
            this.watcher = FileSystems.getDefault().newWatchService();
        } catch (IOException e) {
            logger.error(String.format("Error creating directory watcher: %s", e.getMessage()));
            System.exit(1);
        }
    }

    public void register() {
        try {
            this.key = this.watchDir.register(watcher, ENTRY_CREATE);
        } catch (IOException e) {
            logger.error(String.format("Error registering directory: %s. %s", this.watchDir, e.getMessage()));
        }
    }

    public void unregister() {
        this.key.cancel();
    }

    public void listen() throws InterruptedException {
        for (; ; ) {
            WatchKey key = watcher.take();

            @SuppressWarnings("unchecked")
            Path askit = key.pollEvents().stream()
                    .filter(ev -> ev.kind() == ENTRY_CREATE)
                    .filter(ev -> this.fileName.equals(((WatchEvent<Path>) ev).context().toString()))
                    .map(ev -> (Path) ev.context())
                    .findFirst()
                    .orElse(null);

            Path child = this.watchDir.resolve(askit);

            // Sleep for a little bit to let the fs release any locks on the file
            Thread.sleep(100);
            this.delete(child);

            boolean valid = key.reset();
            if (!valid) {
                break;
            }
        }
    }

    public void delete(Path delete) {
        try {
            Files.deleteIfExists(delete);
            logger.info(String.format("File deleted: %s", delete));
        } catch (IOException e) {
            logger.error(String.format("Cannot delete file: %s", delete));
        }
    }

    /**
     * Start the directory watcher daemon in the background as a thread.
     */
    public void startDaemon() {
        if (this.daemon != null) {
            logger.info("Directory watcher already started");
            return;
        }

        this.daemon = new Thread(() -> {
            try {
                this.listen();
            } catch (InterruptedException e) {
                logger.info("Directory watcher stopped");
                return;
            }
        });

        this.daemon.start();
        logger.info("Directory watcher started");
    }

    /**
     * Stops the daemon by interrupting it.
     */
    public void stopDaemon() {
        if (this.daemon == null) {
            logger.info("Directory watcher not yet started!");
            return;
        }

        this.daemon.interrupt();
        this.daemon = null;
    }


    /**
     * The main program. Takes in the arguments and calls the required methods.
     *
     * @param args The first argument is the absolute or relative directory to watch.
     */
    public static void main(String[] args) {
        Options options = new Options();
        options.addOption("d", "watchdir", true, "The directory to watch");
        options.addOption(Option.builder("f").longOpt("filename").required(false).hasArg().build());
        options.addOption(Option.builder("i").longOpt("interactive").required(false).build());
        options.addOption(Option.builder("h").longOpt("help").required(false).build());

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
            if (cmd.hasOption("h")) {
                throw new ParseException("help");
            }
        } catch (ParseException e) {
            usage();
            System.exit(1);
        }

        Path watchDir = Paths.get(cmd.getOptionValue("d", DEFAULT_WATCH_DIR)).toAbsolutePath().normalize();
        String fileName = cmd.getOptionValue("f", DEFAULT_FILE_NAME);
        Boolean interactive = cmd.hasOption("i");

        Path askit = watchDir.resolve(fileName).toAbsolutePath().normalize();
        try {
            boolean deleted = Files.deleteIfExists(askit);
            if (deleted) {
                logger.info(String.format("File deleted: %s", askit));
            } else {
                logger.info("No file present");
            }
        } catch (IOException e) {
            logger.error(String.format("Cannot delete file: %s", askit));
        }


        AskITRemover watcher = new AskITRemover(watchDir, fileName);
        watcher.register();

        if (interactive) {
            try (Scanner sc = new Scanner(System.in)) {
                String line;
                for (; ; ) {
                    System.out.print("> ");
                    line = sc.nextLine();
                    if (line.equalsIgnoreCase("start")) {
                        logger.info("Starting watcher daemon...");
                        watcher.startDaemon();
                    } else if (line.equalsIgnoreCase("stop")) {
                        logger.info("Stopping watcher daemon...");
                        watcher.stopDaemon();
                    } else if (line.equalsIgnoreCase("quit")) {
                        watcher.stopDaemon();
                        break;
                    } else {
                        logger.info("Usage: start|stop|quit");
                    }
                }
            }
        } else {
            try {
                logger.info("Watching directory for changes...");
                watcher.listen();
            } catch (InterruptedException e) {
                return;
            }
        }
        watcher.unregister();
    }

    /**
     * Abstraction of the usage command.
     */
    private static void usage() {
        logger.error("Usage: java AskITRemover [-i|--interactive] [-f|--filename <filename>] [-d|--watchdir <watchdir>] ");
        logger.error("Defaults: interactive: false, filename: AskIT.url, watchdir: %PUBLIC%\\Desktop");
        System.exit(1);
    }

}