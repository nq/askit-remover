call .\mvnw package

echo "Making directories"
if not exist "%APPDATA%\askit-remover" mkdir "%APPDATA%\askit-remover"

echo "Copying files"
copy .\target\askit-remover-0.1.jar "%APPDATA%\askit-remover"
copy .\bin\nssm.exe "%APPDATA%\askit-remover"

echo "Setting environment variables"
FOR /F %%a IN ('where javaw') DO SET java=%%a

echo "Installing and configuring service"
"%APPDATA%\askit-remover\nssm.exe" install "AskIT Remover" "%java%" "-jar %APPDATA%\askit-remover\askit-remover-0.1.jar"
"%APPDATA%\askit-remover\nssm.exe" set "AskIT Remover" AppDirectory "%APPDATA%\askit-remover"

echo "Starting service"
net start "AskIT Remover"
